fullfield = elementObj.name.replaceAll(" ", "_").replaceAll("-", "_").replaceAll("'", "_").replaceAll(".", "_").replaceAll("’","_").replaceAll("(","_").replaceAll(")","_").replaceAll("ʼ","_") + " = Language{Name: `" + elementObj.name + "`, Type: `" + ((elementObj.type === null) ? "-" : elementObj.type) + "`, Scope: `" + ((elementObj.scope === null) ? "-" : elementObj.scope) + "`, ISO_639_3: `" + ((elementObj.iso6393 === null) ? "-" : elementObj.iso6393) + "`, ISO_639_2_B: `" + ((elementObj.iso6392B === null) ? "-" : elementObj.iso6392B) + "`, ISO_639_2_T: `" + ((elementObj.iso6392T === null) ? "-" : elementObj.iso6392T) + "`, ISO_639_1: `" + ((elementObj.iso6391 === null) ? "-" : elementObj.iso6391) + "`, CustomId: " + elementObj["tandk-language-id"] + "}";
namefield = elementObj.name.replaceAll(" ", "_").replaceAll("-", "_").replaceAll("'", "_").replaceAll(".", "_").replaceAll("’","_").replaceAll("(","_").replaceAll(")","_").replaceAll("ʼ","_")

var jsonLanguage = `INSERT JSON!`;
var collectionLanguage = JSON.parse(jsonLanguage);
var request = 'INSERT INTO `languages`("name", "type", "scope", "iso6393", "iso6392B", "iso6392T", "iso6391", "tandk-language-id") VALUES';

collectionLanguage.forEach(language => {
    request += "('" + language["name"].replaceAll("'", "''") + "', " + ((language["type"] === null) ? "NULL" : "'" + language["type"] + "'") + ", " + ((language["scope"] === null) ? "NULL" : "'" + language["scope"] + "'") + ", " + ((language["iso6393"] === null) ? "NULL" : "'" + language["iso6393"] + "'") + ", " + ((language["iso6392B"] === null) ? "NULL" : "'" + language["iso6392B"] + "'") + ", " + ((language["iso6392T"] === null) ? "NULL" : "'" + language["iso6392T"] + "'") + ", " + ((language["iso6391"] === null) ? "NULL" : "'" + language["iso6391"] + "'") + ", " + language["tandk-language-id"] + "),\n"; 
});

console.log(request.slice(0, request.length - 2) + ";");