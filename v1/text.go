package v1

type Text struct {
	English string
	Russian string
	Default string `json:"default"`
}

func (text *Text) GetText(language *Language) *string {
	switch language.Id {
	case English.Id:
		return &text.English
	case Russian.Id:
		return &text.Russian
	default:
		return &text.Default
	}
}

func (text *Text) GetTextByLanguageName(name *string) *string {
	switch *name {
	case English.Name:
		return &text.English
	case Russian.Name:
		return &text.Russian
	default:
		return &text.Default
	}
}

func (text *Text) GetTextByISO_639_3(code *string) *string {
	switch *code {
	case English.ISO_639_3:
		return &text.English
	case Russian.ISO_639_3:
		return &text.Russian
	default:
		return &text.Default
	}
}

func (text *Text) GetTextByISO_639_2B(code *string) *string {
	switch *code {
	case English.ISO_639_2_B:
		return &text.English
	case Russian.ISO_639_2_B:
		return &text.Russian
	default:
		return &text.Default
	}
}

func (text *Text) GetTextByISO_639_2T(code *string) *string {
	switch *code {
	case English.ISO_639_2_T:
		return &text.English
	case Russian.ISO_639_2_T:
		return &text.Russian
	default:
		return &text.Default
	}
}

func (text *Text) GetTextByISO_639_1(code *string) *string {
	switch *code {
	case English.ISO_639_1:
		return &text.English
	case Russian.ISO_639_1:
		return &text.Russian
	default:
		return &text.Default
	}
}

func (text *Text) GetTextById(id *int) *string {
	switch *id {
	case English.Id:
		return &text.English
	case Russian.Id:
		return &text.Russian
	default:
		return &text.Default
	}
}
